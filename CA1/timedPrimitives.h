#ifndef PRIMITIVES_H
#define PRIMITIVES_H

	#include "definitions.h"

	void and1 (tlogic a, tlogic b, tlogic &w);
	void or1 (tlogic a, tlogic b, tlogic &w);
	void not1 (tlogic a, tlogic &w);
	void xor1 (tlogic a, tlogic b, tlogic &w);

#endif