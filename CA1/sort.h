#ifndef SORT_H
#define SORT_H
	
	#include "definitions.h"

	typedef std::vector<std::string> Gate;
	typedef std::vector<Gate> Gates;

	void sort_gates(Gates &gates);

#endif