#include "parser.h"

void replaceComa(std::string &s) {
  replace( s.begin(), s.end(), ',', ' '); // replace all ',' to ' '
}

void replaceSemi(std::string &s) {
  replace( s.begin(), s.end(), ';', ' '); // replace all ';' to ' '
}

void replaceParan(std::string &s) {
  replace( s.begin(), s.end(), '(', ' '); // replace all '(' to ' '
  replace( s.begin(), s.end(), ')', ' '); // replace all ')' to ' '
}

void replaceSpace(std::string &s) {
  replace( s.begin(), s.end(), ' ', '\0'); // replace all ' ' to ''
}

Component parser(std::string &s) {
	Component c;
	replaceComa(s); replaceSemi(s); replaceParan(s);
	std::istringstream iss(s);   
  	std::copy(std::istream_iterator<std::string>(iss),
        std::istream_iterator<std::string>(),
        std::back_inserter(c));
  	return c;
}