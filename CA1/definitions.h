#ifndef DEFINITIONS_H
#define DEFINITIONS_H

	#include <iostream>
	#include <string>
	#include <vector>
	#include <sstream>
	#include <fstream>
	#include <algorithm>
	#include <iterator>
	#include <cmath> 

	struct tlogic {
		char logic;
		std::string name;
		int time;
	};

	typedef std::vector<std::string> Component;
	typedef std::vector<Component> Gates;
	typedef std::vector<Component> Wires;
	typedef std::vector<Component> ModuleHeader;
	typedef std::vector<tlogic> Logics;
	// typedef std::vector<Logics> Routes;

#endif