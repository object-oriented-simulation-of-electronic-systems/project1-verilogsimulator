#include "io.h"

void input(std::string file_name, ModuleHeader &moduleHeader, Wires &wires, Gates &gates) {
	Component c;
	std::string s;
	std::ifstream fin (file_name.c_str());
	while(getline(fin, s)) {
		c = parser(s);
		if(c.size() > 0) {
			if(c[0] == "wire")
				wires.push_back(c);
			else if(c[0] == "module")
				moduleHeader.push_back(c);
			else if(c[0] != "endmodule")
				gates.push_back(c);
		}
		c.clear();
	}
}

void gate_ptint(Gates g) {
	for(int i = 0; i < g.size(); i++) {
		for(int j = 0; j < g[i].size(); j++) {
			std::cout << g[i][j] << "::";
		}
		std::cout << std::endl;
	}
}

int find_input_size(Logics &logics) {
	int n = 0;
	for(int i = 1; i < logics.size(); i++) {
		if(logics[i].name == "output")
			return n;
		n++;
	}
}

int find_output_size(Logics &logics) {
	int n = 0;
	for(int i = find_input_size(logics) + 2; i < logics.size(); i++) {
		if(logics[i].name == "wire")
			return n;
		n++;
	}
}

void print_header(Logics &logics) {
	int input_size = find_input_size(logics);
	int output_size = find_output_size(logics);
	std::string output_file_name = "out";
	std::ofstream fout (output_file_name.c_str(), std::ios::app);
	// inputs
	std::cout << "{";	fout << "{";
	for(int i = 1; i < input_size + 1; i++) {
		if(i == input_size) {
		 	std::cout << logics[i].name;	fout << logics[i].name;
		} else {
			std::cout << logics[i].name << ", ";	fout << logics[i].name << ", ";
		}
	}
	std::cout << "}-";	fout << "}-";

	// outputs
	std::cout << "{";	fout << "{";
	int output_end = input_size + 2 + output_size;
	for(int i = input_size + 2; i < output_end; i++) {
		if(i == output_end - 1) {
		 	std::cout << logics[i].name;	fout << logics[i].name;
		} else {
			std::cout << logics[i].name << ", ";	fout << logics[i].name << ", ";
		}
	}
	std::cout << "}";	fout << "}";
}

void print_row(Logics &logics) {
	int input_size = find_input_size(logics);
	int output_size = find_output_size(logics);
	std::string output_file_name = "out";
	std::ofstream fout (output_file_name.c_str(), std::ios::app);
	// inputs
	std::cout << "{";	fout << "{";
	for(int i = 1; i < input_size + 1; i++) {
		if(i == input_size) {
		 	std::cout << logics[i].logic << " ";	fout << logics[i].logic << " ";
		} else {
			std::cout << logics[i].logic << ", ";	fout << logics[i].logic << ", ";
		}
	}
	std::cout << "}-";	fout << "}-";

	// outputs
	std::cout << "{";	fout << "{";
	int output_end = input_size + 2 + output_size;
	for(int i = input_size + 2; i < output_end; i++) {
		if(i == output_end - 1) {
		 	std::cout << logics[i].logic << " ";	fout << logics[i].logic << " ";
		} else {
			std::cout << logics[i].logic << ", ";	fout << logics[i].logic << ", ";
		}
	}
	std::cout << "}";	fout << "}";
}

void print_wires_header(Logics &logics) {
	int wires_head = find_input_size(logics) + find_output_size(logics) + 3;
	std::string output_file_name = "out";
	std::ofstream fout (output_file_name.c_str(), std::ios::app);
	std::cout << "\t{";	fout << "\t{";
	for(int i = wires_head; i < logics.size(); i++) {
		if(i == logics.size() - 1) {
		 	std::cout << logics[i].name << " ";		fout << logics[i].name;
		} else {
			std::cout << logics[i].name << ", ";	fout << logics[i].name << ", ";
		}
	}
	std::cout << "}" << std::endl;	fout << "}" << std::endl;
}

void print_wires_value(Logics &logics) {
	int wires_head = find_input_size(logics) + find_output_size(logics) + 3;
	std::string output_file_name = "out";
	std::ofstream fout (output_file_name.c_str(), std::ios::app);
	std::cout << "\t{";	fout << "\t{";
	for(int i = wires_head; i < logics.size(); i++) {
		if(i == logics.size() - 1) {
		 	std::cout << logics[i].logic << " ";	fout << logics[i].logic << " ";
		} else {
			std::cout << logics[i].logic << ", ";	fout << logics[i].logic << ", ";
		}
	}
	std::cout << "}" << std::endl;	fout << "}" << std::endl;
}