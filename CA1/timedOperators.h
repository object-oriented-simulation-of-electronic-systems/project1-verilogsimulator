#ifndef OPERATORS_H
#define OPERATORS_H

	#include "definitions.h"

	tlogic operator& (tlogic a, tlogic b);
	tlogic operator| (tlogic a, tlogic b);
	tlogic operator~ (tlogic a);
	tlogic operator^ (tlogic a, tlogic b);

#endif