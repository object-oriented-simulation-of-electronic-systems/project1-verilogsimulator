#include "timedOperators.h"
#include "timedPrimitives.h"
#include "parser.h"
#include "io.h"
#include "sort.h"
#include "execution.h"
using namespace std;

int main() {
	string file_name;
	cout << "Enter input file name : " << endl;
	cin >> file_name;
	while(file_name != "exit") {
		Gates gates;
		Wires wires;
		ModuleHeader moduleHeader;
		Logics logics;
		input(file_name, moduleHeader, wires, gates);
		sort_gates(gates);
		wire_maker(moduleHeader, wires, logics);
		set_wire_value("a", '1', logics);
		testbench(logics, gates, moduleHeader);
		cout << "Enter input file name or 'exit' to finish program" << endl;
		cin >> file_name;
	}
	
	return 0;
}