#include "timedOperators.h"

tlogic operator& (tlogic a, tlogic b) {
	tlogic t1;
	if(a.logic == '0' || b.logic == '0') t1.logic = '0';
	else if(a.logic == '1' && b.logic == '0') t1.logic = '1';
	else t1.logic = 'x';
	return t1;
}

tlogic operator| (tlogic a, tlogic b) {
	tlogic t1;
	if(a.logic == '1' || b.logic == '1') t1.logic = '1';
	else if(a.logic == '0' && b.logic == '0') t1.logic = '0';
	else t1.logic = 'x';
	t1.time = a.time;
	return t1;
}

tlogic operator~ (tlogic a) {
	tlogic t1;
	if(a.logic == '1') t1.logic = '0';
	else if(a.logic == '0') t1.logic = '1';
	else t1.logic = 'x';
	t1.time = a.time;
	return t1;
}

tlogic operator^ (tlogic a, tlogic b) {
	tlogic t1;
	if(a.logic == b.logic) t1.logic = '0';
	else t1.logic = '1';
	if(a.time > b.time) t1.time = a.time;
	else t1.time = b.time;
	return t1;
}
