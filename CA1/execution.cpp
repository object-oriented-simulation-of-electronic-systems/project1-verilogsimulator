#include "execution.h"

void input_wire_maker(ModuleHeader &moduleHeader, Logics &logics) {
	tlogic temp;
	temp.logic = 'x';
	temp.name = "input";
	logics.push_back(temp);
	for(int i = 2; i < moduleHeader[0].size(); i++) {
		if(moduleHeader[0][i] == "input") {
			i++;
			while(i < moduleHeader[0].size() && moduleHeader[0][i] != "output") {
				temp.name = moduleHeader[0][i];
				logics.push_back(temp);
				i++;
			}
		}
	}
}

void output_wire_maker(ModuleHeader &moduleHeader, Logics &logics) {
	tlogic temp;
	temp.logic = 'x';
	temp.name = "output";
	logics.push_back(temp);
	for(int i = 2; i < moduleHeader[0].size(); i++) {
		if(moduleHeader[0][i] == "output") {
			i++;
			while(i < moduleHeader[0].size() && moduleHeader[0][i] != "input") {
				temp.name = moduleHeader[0][i];
				logics.push_back(temp);
				i++;
			}
		}
	}
}

void inside_wire_maker(Wires &wires, Logics &logics) {
	tlogic temp;
	temp.logic = 'x';
	temp.name = "wire";
	logics.push_back(temp);
	for(int j = 0; j < wires.size(); j++) {
		for(int k = 1; k < wires[j].size(); k++) {
			temp.name = wires[j][k];
			logics.push_back(temp);
		}
	}
}

void wire_maker(ModuleHeader &moduleHeader, Wires &wires, Logics &logics) {
	input_wire_maker(moduleHeader, logics);
	output_wire_maker(moduleHeader, logics);
	inside_wire_maker(wires, logics);
}

int find_wire(std::string wireName, Logics &logics) {
	for(int i = 0; i < logics.size(); i++) {
		if(logics[i].name == wireName){
			return i;
		}
	}
}

void set_wire_value(std::string wireName, char value, Logics &logics) {
	logics[find_wire(wireName, logics)].logic = value;
}

void testbench(Logics &logics, Gates &gates, ModuleHeader &moduleHeader) {
	std::cout << "choose one of the options : " << "1) a full testbench\t2) enter inputs\t0) end" << std::endl;
	int type = 0;
	std::cin >> type;
	while(type != 0) {
		if(type == 1) {
			print_header(logics);
			print_wires_header(logics);
			for(int i = 0; i < pow(2, find_input_size(logics)); i++) {
				set_inputs(logics, toBinary(i, find_input_size(logics)));
				simulate(logics, gates);
			}
		} else if(type == 2) {
			std::cout << "enter your inputs Continuous : " << std::endl;
			std::string inputs;
			std::cin >> inputs;
			if(inputs.length() != find_input_size(logics)) {
				std::cout << "data missing!! try again :)" << std::endl;
			} else {
				print_header(logics);
				print_wires_header(logics);
				set_inputs(logics, inputs);
				simulate(logics, gates);
			}
			
		}
		std::string output_file_name = "out";
		std::ofstream fout (output_file_name.c_str(), std::ios::app);
		fout << std::endl;
		std::cout << std::endl << "choose one of the options : " << "1) a full testbench\t2) enter inputs\t0) end" << std::endl;
		std::cin >> type;
	}
}

std::string toBinary(int n, int size) {
    std::string r;
    std::string result;
    while(n != 0) { r = ( n % 2 == 0 ? "0" : "1" ) + r; n /= 2; }
    result = r;
    for(int i = 0; i < size - r.length(); i++) {
    	result = "0" + result;
    }
    return result;
}

void set_inputs(Logics &logics, std::string input) {
	for(int i = 1; i < find_input_size(logics) + 1; i++) {
		logics[i].logic = input[i-1];
	}
}

void simulate(Logics &logics, Gates &gates) {
	for(int i = 0; i < gates.size(); i++) {
		if(gates[i][0] == "and") {
			and1(logics[find_wire(gates[i][2], logics)], logics[find_wire(gates[i][3], logics)], logics[find_wire(gates[i][1], logics)]);
		} else if(gates[i][0] == "or") {
			or1(logics[find_wire(gates[i][2], logics)], logics[find_wire(gates[i][3], logics)], logics[find_wire(gates[i][1], logics)]);
		} else if(gates[i][0] == "xor") {
			xor1(logics[find_wire(gates[i][2], logics)], logics[find_wire(gates[i][3], logics)], logics[find_wire(gates[i][1], logics)]);
		} else if(gates[i][0] == "not") {
			not1(logics[find_wire(gates[i][2], logics)], logics[find_wire(gates[i][1], logics)]);
		}
	}
	print_row(logics);
	print_wires_value(logics);
}
