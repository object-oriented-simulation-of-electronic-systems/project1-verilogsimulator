#include "timedPrimitives.h"

void and1 (tlogic a, tlogic b, tlogic &w) {
	if(a.logic == '0' || b.logic == '0') w.logic = '0';
	else if(a.logic == '1' && b.logic == '1') w.logic = '1';
	else w.logic = 'x';
}

void or1 (tlogic a, tlogic b, tlogic &w) {
	if(a.logic == '1' || b.logic == '1') w.logic = '1';
	else if(a.logic == '0' && b.logic == '0') w.logic = '0';
	else w.logic = 'x';
	w.time = a.time;
}

void not1 (tlogic a, tlogic &w) {
	if(a.logic == '1') w.logic = '0';
	else if(a.logic == '0') w.logic = '1';
	else w.logic = 'x';
	w.time = a.time;
}

void xor1 (tlogic a, tlogic b, tlogic &w) {
	if(a.logic == b.logic) w.logic = '0';
	else w.logic = '1';
	if(a.time > b.time) w.time = a.time;
	else w.time = b.time;
}
