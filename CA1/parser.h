#ifndef PARSER_H
#define PARSER_H
	
	#include "definitions.h"

	void replaceComa (std::string &s);
	void replaceSemi (std::string &s);
	void replaceParan (std::string &s);
	void replaceSpace (std::string &s);

	Component parser(std::string &s);

#endif