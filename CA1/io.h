#ifndef IO_H
#define IO_H

	#include "parser.h"

	void input(std::string file_name, ModuleHeader &moduleHeader, Wires &wires, Gates &gates);
	void gate_ptint(Gates g);
	int find_input_size(Logics &logics);
	int find_output_size(Logics &logics);
	void print_header(Logics &logics);
	void print_row(Logics &logics);
	void print_wires_header(Logics &logics);
	void print_wires_value(Logics &logics);

#endif