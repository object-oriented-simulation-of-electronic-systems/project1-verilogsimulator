#include "sort.h"

void sort_gates(Gates &gates) {
	for(int i = 0; i < gates.size(); i++) {
		for(int j = 0; j < gates.size(); j++) {
			// std::cout << gates[i][0] << "," << gates[i][1] << ":::::" << gates[j][2] << "," << gates[j][3] << std::endl;
			if(gates[i][1] == gates[j][2] || gates[i][1] == gates[j][3]) {
				std::swap(gates[i], gates[j]);
			}
		}
	}
}