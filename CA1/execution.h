#ifndef EXECUTION_H
#define EXECUTION_H
	
	#include "timedPrimitives.h"
	#include "io.h"

	void wire_maker(ModuleHeader &moduleHeader, Wires &wires, Logics &logics);
	void input_wire_maker(ModuleHeader &moduleHeader, Logics &logics);
	void output_wire_maker(ModuleHeader &moduleHeader, Logics &logics);
	void inside_wire_maker(Wires &wires, Logics &logics);
	void set_wire_value(std::string wireName, char value, Logics &logics);
	int find_wire(std::string wireName, Logics &logics);
	void testbench(Logics &logics, Gates &gates, ModuleHeader &moduleHeader);
	std::string toBinary(int n, int size);
	void set_inputs(Logics &logics, std::string input);
	void simulate(Logics &logics, Gates &gates);

#endif